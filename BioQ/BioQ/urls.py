"""projet_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import url
from projetdjangoapp.views import foo, view_table, signup,quizz1, quizz2, In, Out, registered, categories1, categories2
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
                  url(r'', admin.site.urls),
                  re_path(r'^BioQ/$', foo, name='index'),
                  re_path(r'^BioQ/login/$', LoginView.as_view(redirect_authenticated_user=True), name='login'),
                  url(r'^BioQ/logout/$', LogoutView.as_view(), name='logout'),
                  url(r'^BioQ/explore$', view_table),
                  # Registration URLs
                  url(r'^BioQ/registration$', signup),
                  url(r'^BioQ/quizz1$', quizz1),
                  url(r'^BioQ/quizz2$', quizz2),
                  url(r'^BioQ/loggedIn/$', In),
                  url(r'^BioQ/loggedOut/$', Out),
                  url(r'^BioQ/registered/$', registered),
                  url(r'^BioQ/categories1/$', categories1),
                  url(r'^BioQ/categories2/$', categories2),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)