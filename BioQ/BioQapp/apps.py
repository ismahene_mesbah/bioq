from django.apps import AppConfig


class BioQappConfig(AppConfig):
    name = 'BioQapp'
