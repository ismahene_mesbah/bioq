from django.shortcuts import render
from projetdjangoapp.models import Image, Question, Answers, Utilisateur
from numpy import random


def foo(request,):
    return render(request, "DJ/home.html",
       {"Testing" : "Django Template Inheritance ",
       "HelloHello" : "Hello World - Django"})

def In(request,):
    return render(request, "registration/In.html")

def Out(request,):
    return render(request, "registration/Out.html")

def categories1(request,):
    return render(request, "DJ/categories1.html")

def categories2(request,):
    return render(request, "DJ/categories2.html")

def view_table(request,):
    Image.deleteAll()
    Image.ImportData()
    objs=Image.objects.all()
    return render(request,'DJ/explore.html',{'objs':objs})

from django.http import HttpResponseRedirect

def registered(request, ):
    return render(request, "registration/registered.html")


from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .forms import SignUpForm
from django.conf import settings


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/BioQ/registered')
    else:
        form = SignUpForm()
    return render(request, 'registration/registration.html', {'form': form})

import random
from django.views.decorators.csrf import csrf_exempt
@csrf_exempt
def quizz1(request):
    current_user = request.user
    user = Utilisateur.objects.get(user_id=current_user.id)
    score=user.score
    name =user.user.username
    Answers.deleteAll()
    Answers.ImportData()
    opts=Answers.objects.filter(question_id=1)
    Question.deleteAll()
    Question.ImportData()
    ques=Question.objects.get(type='microscopy')
    Image.deleteAll()
    Image.ImportData()
    modes=['fluorescence microscopy', 'scanning electron microscopy (SEM)', 'transmission electron microscopy (TEM)', 'phase contrast microscopy', 'illumination by electrons']
    for i in modes:
        img=Image.objects.filter(mode=i)
        ans=Answers.objects.filter(answer=i)
        mode=i
        random.shuffle(modes)
    img = img.order_by('?')[:3]
    current_user = request.user
    if request.method == 'POST':
        user = Utilisateur.objects.get(user_id=current_user.id)
        user.score = user.score +  int(request.POST['counter'])
        user.save()
       # print(user.score)
    return render(request,'DJ/quizz1.html',{'score':score, 'name':name,'opts':opts, 'ques':ques, 'img':img, 'mode':mode, 'ans': ans})

from django.db.models import Q
@csrf_exempt
def quizz2(request):
    current_user = request.user
    user = Utilisateur.objects.get(user_id=current_user.id)
    score=user.score
    name =user.user.username
    Answers.deleteAll()
    Answers.ImportData()

    Question.deleteAll()
    Question.ImportData()
    ques = Question.objects.get(type='component')
    Image.deleteAll()
    Image.ImportData()
    components= ['pollen wall', 'dendrite', 'synaptic vesicle', 'desmosome', 'axoneme', 'endoplasmic', 'mitochondrion']
    for i in components:
        img = Image.objects.filter(component=i)
        ans = Answers.objects.filter(answer=i)
        component = i
        random.shuffle(components)
    img = img.order_by('?')[:2]
    opts = Answers.objects.filter(question_id=2).filter(~Q(answer=component))[:3]
    current_user = request.user
    if request.method == 'POST':
        user = Utilisateur.objects.get(user_id=current_user.id)
        user.score = user.score + int(request.POST['counter'])
        user.save()
        print(user.score)

    return render(request,'DJ/quizz2.html', {'name':name, 'score':score,'opts':opts, 'ques':ques, 'img':img, 'component': component, 'ans':ans })

