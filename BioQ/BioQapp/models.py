from django.db import models
import os.path

import csv

# Create your models here.
class Image(models.Model):
    name = models.IntegerField()  # field - instance - row
    description = models.TextField(max_length=700)  # field - instance - row
    mode = models.CharField(max_length=255)  # field - instance - row
    celltype = models.CharField(max_length=255)  # field - instance - row
    component = models.CharField(max_length=255)  # field - instance - row
    doi = models.CharField(max_length=255)  # field - instance - row
    organism = models.CharField(max_length=255)  # field - instance - row
    def deleteAll():
        for img in Image.objects.all():
            img.delete()

    def ImportData():
        my_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(my_path, "./static/Images.csv")
        reader = csv.reader(open(path,encoding='utf-8'), delimiter=",", quotechar='"')
        next(reader)
        for row in reader:
            print(row)
            image = Image()
            image.name = row[1]
            image.description = row[2]
            image.mode = row[3]
            image.celltype = row[4]
            image.component = row[5]
            image.doi = row[6]
            image.organism = row[7]
            image.save()


class Question(models.Model):
    question= models.CharField(max_length=255)
    type= models.CharField(max_length=255)
    imagefield= models.CharField(max_length=255)
    point=models.CharField(max_length=255)
    n_answer= models.IntegerField()
    n_image= models.IntegerField()
    image = models.ForeignKey(Image, on_delete=models.CASCADE, null=True)

    def deleteAll():
        for qq in Question.objects.all():
            qq.delete()

    def ImportData():
        my_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(my_path, "./static/Questions.csv")
        reader = csv.reader(open(path, encoding='utf-8'), delimiter=",", quotechar='"')
        next(reader)
        for row in reader:
            question= Question()
            question.question = row[1]
            question.type = row[2]
            question.imagefield = row[3]
            question.point = row[4]
            question.n_answer = row[5]
            question.n_image = row[6]
            question.save()


class Answers(models.Model):
    question_id= models.IntegerField()
    answer= models.CharField(max_length=255)
    definition= models.TextField(max_length=255)
    def deleteAll():
        for ans in Answers.objects.all():
            ans.delete()
    def ___str__(self):
        return self.answer
    def ImportData():
        my_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(my_path, "./static/Answers.csv")
        reader = csv.reader(open(path, encoding='utf-8'), delimiter=",", quotechar='"')
        next(reader)
        for row in reader:
            answer= Answers()
            answer.question_id= row[1]
            answer.answer = row[2]
            answer.definition = row[3]
            answer.save()

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver



class Utilisateur(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,related_name='profile')
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    score = models.IntegerField( default=0)

from django.core.exceptions import ObjectDoesNotExist

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    try:
        instance.profile.save()
    except ObjectDoesNotExist:
        Utilisateur.objects.create(user=instance)





# reset migrations:
# find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
# find . -path "*/migrations/*.pyc"  -delete

